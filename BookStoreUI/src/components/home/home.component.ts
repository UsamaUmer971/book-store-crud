import { HttpClient , HttpParams} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataService } from 'src/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  apiData:any;
  a :any;
  errorMessage = '';
  editData=false;

  constructor(private fb: FormBuilder,private dataService:DataService,  private http: HttpClient) { }
  profileForm = this.fb.group({
    booktitle: ['',Validators.required],
    bookauthor: [''],
    bookdescription:[''],
    id:['']
  });
  ngOnInit(): void {
    this.getBooks();
  }
  getBooks(){
    this.dataService.getBooksData().subscribe((data:any)=>{
      console.log(data);
      this.apiData=data;
    });
  }
  reload(){
    window.location.reload();
  }
  remove(id:any){
    // let params = new HttpParams().set("id",id);
    this.dataService.deleteBook(id).subscribe((a:any)=>{
      alert("Delete Successfully !!!");
      window.location.reload();
    });
  }
  
  onSubmit(){
    console.log(this.profileForm.value);
    this.dataService.addBook(this.profileForm.value).subscribe(
      data => {
        console.log(data);
        window.location.reload();
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );
  }

  
  fetchData(data:any){
    this.editData=true;
    // console.log(data.id);
    this.profileForm.controls['booktitle'].setValue(data.booktitle);
    this.profileForm.controls['bookdescription'].setValue(data.bookdescription);
    this.profileForm.controls['bookauthor'].setValue(data.bookauthor);
    this.profileForm.controls['id'].setValue(data.id);

  }
  UpdateData(){
    this.dataService.updateBook(this.profileForm.value,this.profileForm.value.id).subscribe(
      data => {
        console.log(data);
        window.location.reload();
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );
  }

}
