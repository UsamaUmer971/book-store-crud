import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { DataService } from 'src/services/data.service';
@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {
  errorMessage = '';
  constructor(private fb: FormBuilder, private dataService:DataService) { }
  profileForm = this.fb.group({
    booktitle: ['',Validators.required],
    bookauthor: [''],
    bookdescription:['']
  });
  ngOnInit(): void {
  }
  
  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.log(this.profileForm.value);
    this.dataService.addBook(this.profileForm.value).subscribe(
      data => {
        console.log(data);
        window.location.reload();
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );
  }
  fetchData(data:any){
    this.profileForm.controls['booktitle'].setValue(data.booktitle);
  }
  
}
