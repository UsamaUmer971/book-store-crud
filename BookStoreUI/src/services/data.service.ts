import { Injectable } from '@angular/core';
import { HttpClient ,HttpResponse ,HttpHeaders,HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  API_URL ="http://localhost:8080/"
  constructor(private http: HttpClient) { }
  getBooksData(){
    return this.http.get(this.API_URL+'book');
  }
  addBook(data:any): Observable<any> {
    return this.http.post(this.API_URL + 'book', data);
  }
  deleteBook(deleteId:any){
    return this.http.delete(this.API_URL + 'book/'+deleteId);
  }
  updateBook(data:any, id:any){
    return this.http.put<any>(this.API_URL+'book/'+id,data)
  }
  
}
