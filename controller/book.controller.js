const db = require("../model");
const Book = db.bookDetails;
// Create
exports.create = (req, res) => {
    // Validate request
    if (!req.body.booktitle) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    // Create a book
    const book = new Book({
        booktitle: req.body.booktitle,
        bookauthor: req.body.bookauthor,
        bookdescription: req.body.bookdescription
    });
  
    // Save book in the database
    book
      .save(book)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the ."
        });
      });
  };
// Find All
exports.findAll = (req, res) => {
    const booktitle = req.query.booktitle;
    var condition = booktitle ? { booktitle: { $regex: new RegExp(booktitle), $options: "i" } } : {};
  
    Book.find(condition)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving ."
        });
      });
  };
// Find by Id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Book.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving  with id=" + id });
    });
}; 
// Update
exports.update = (req, res) => {
    if (!req.body) {
      return res.status(400).send({
        message: "Data to update can not be empty!"
      });
    }
  
    const id = req.params.id;
  
    Book.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
      .then(data => {
        if (!data) {
          res.status(404).send({
            message: `Cannot update  with id=${id}. Maybe Book was not found!`
          });
        } else res.send({ message: "Book was updated successfully." });
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Book with id=" + id
        });
      });
  };
//   Delete
exports.delete = (req, res) => {
    const id = req.params.id;
    Book.findByIdAndRemove(id)
      .then(data => {
        if (!data) {
          res.status(404).send({
            message: `Cannot delete Book with id=${id}. Maybe Book was not found!`
          });
        } else {
          res.send({
            message: "Book was deleted successfully!"
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Book with id=" + id
        });
      });
  };
//   Delete All
exports.deleteAll = (req, res) => {
    Book.deleteMany({})
      .then(data => {
        res.send({
          message: `${data.deletedCount} Books were deleted successfully!`
        });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all Books."
        });
      });
  };