// Create
module.exports = mongoose => {
    var schema = mongoose.Schema(
      {
        booktitle: String,
        bookauthor: String,
        bookdescription: String,
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    }); 
  
    const Books = mongoose.model("booksDetails", schema);
    return Books;
  };
  